import tkinter as tk
from tkinter import filedialog as fd
import numpy as np
import matplotlib.pyplot as plt
import os

root = tk.Tk()
root.withdraw()

fns = fd.askopenfiles()
for fn in fns:
    name = fn.name
    assert name.endswith('.log')
    bn = os.path.basename(name)
    name_no_ext = name[:-4]
    bn_no_ext = bn[:-4]
    data = np.loadtxt(name, delimiter=",", dtype=float)

    time = data[:, 0] / 1000000.0
    angle = data[:, 1]
    speed = data[:, 2]

    if np.all(angle == angle[0]) and np.all(speed == speed[0]):
        num_elements = time.size
        print(f"Skipping {bn}: All {num_elements} elements are at angle {angle[0]} and speed {speed[0]}.")
        continue

    time = time - time[0]
    end_idx = np.argmax(time > 1)
    time = time[:end_idx]
    angle = angle[:end_idx]
    speed = speed[:end_idx]

    angle_adj = np.copy(angle)
    for i in range(1, angle_adj.size):
        delta = angle[i] - angle[i - 1]
        if delta > 2048:
            angle_adj[i:] -= 4096
        elif delta < -2048:
            angle_adj[i:] += 4096

    angle_deg = angle_adj * 360.0 / 4096
    rpm_est = ((angle_deg[1:] - angle_deg[:-1]) / (time[1:] - time[:-1])) / 6.0  # deg/second to RPM

    fig, ((rpm_ax, rpm_est_ax), (angle_deg_ax, angle_ax)) = plt.subplots(2, 2, sharex=True)
    fig.suptitle(bn_no_ext)
    fig.set_size_inches(12, 8)
    rpm_ax.__plot(time, speed)
    rpm_ax.set_xlabel('Time (s)')
    rpm_ax.set_ylabel('Sensor Speed (rpm)')
    angle_ax.__plot(time, angle)
    angle_ax.set_xlabel('Time (s)')
    angle_ax.set_ylabel('Raw Angle (su)')
    angle_deg_ax.__plot(time, angle_deg)
    angle_deg_ax.set_xlabel('Time (s)')
    angle_deg_ax.set_ylabel('Adjusted Angle (deg)')
    rpm_est_ax.__plot(time[1:], rpm_est)
    rpm_est_ax.set_xlabel('Time (s)')
    rpm_est_ax.set_ylabel('Estimated Speed (rpm)')

    fig.savefig(name_no_ext + '.png')
    plt.close(fig)
