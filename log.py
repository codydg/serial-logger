import collections
import threading
from enum import Enum
from queue import Queue

import numpy as np
from serial import Serial
from serial.tools.list_ports import comports
from threading import Thread
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter import messagebox as mb
from os.path import isfile
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
import time
from math import pi


expected_speed = 345
ymax_drawn = expected_speed * 1.2


class Command(Enum):
    STOP = 1


def parse_line(line):
    file_time, angle, speed = line.split(',')
    return int(file_time) / 1000000.0, int(angle) * pi / 2048.0, float(speed)


class Plotter:
    def __init__(self, running_average_pts=5):
        self.command_queue = Queue()
        self.data = []
        self.data_lock = threading.Lock()
        self.recent_data = collections.deque(maxlen=running_average_pts)
        self.fig = None
        self.animation = None
        self.update_rate_ms = None
        self.root = None

    def start(self, filename):
        if self.running():
            mb.showerror("Start Error", "Already running")
            return False
        else:
            with self.command_queue.mutex:
                self.command_queue.queue.clear()
            with self.data_lock:
                self.data = []
                self.recent_data.clear()
            self.__start(filename)
            return True

    def stop(self):
        if self.running():
            self.command_queue.put(Command.STOP)

    def running(self):
        return self.fig is not None

    def add_datum(self, datum):
        if self.running():
            datum_time, angle, speed = datum
            with self.data_lock:
                self.recent_data.append(speed)
                speed = sum(self.recent_data) / len(self.recent_data)
                self.data.append((datum_time, angle, speed))

    def set_root(self, root, update_rate_ms):
        self.root = root
        self.update_rate_ms = update_rate_ms

    def update(self):
        if self.running():
            if not self.command_queue.empty():
                if self.command_queue.get() == Command.STOP:
                    self.__finalize()
                    return
            plt.pause(0.001)
        if self.root is not None:
            self.root.after(self.update_rate_ms, self.update)

    def __start(self, filename, max_pts=500):
        self.fig = plt.figure()
        offset = [comp[1:] for comp in filename.split('_') if comp.startswith('x')]
        if len(offset) == 1:
            self.fig.suptitle(offset[0])
        else:
            self.fig.suptitle(filename)
        ax = self.fig.add_subplot(polar=True)
        line, = ax.plot([], [])
        ax.plot(np.linspace(0, 2*pi, 100), [expected_speed]*100)
        ax.set_ylim(ymin=0, ymax=ymax_drawn)
        x_data = collections.deque(maxlen=max_pts)
        y_data = collections.deque(maxlen=max_pts)

        def update(_):
            with self.data_lock:
                data, self.data = self.data, []
            for _, angle, speed in data:
                x_data.append(angle)
                y_data.append(speed)
                line.set_xdata(x_data)
                line.set_ydata(y_data)

            return line,

        self.animation = FuncAnimation(self.fig, update)  # , blit=True)

    def __finalize(self):
        plt.close(self.fig)
        self.fig = None
        self.animation = None
        with self.data_lock:
            self.data = []
            self.recent_data.clear()


class Recorder:
    def __init__(self, plotter: Plotter = None):
        self.thread = None
        self.command_queue = Queue()
        self.plotter = plotter

    def start(self, filename, port, baud=115200, timeout=1):
        if self.running():
            mb.showerror("Start Error", "Already running")
            return False
        else:
            if not port:
                mb.showerror("Port Error", "No Port Selected!")
                return False
            if isfile(filename) and not mb.askokcancel("File Exists", "File exists. Overwrite?"):
                return False
            with self.command_queue.mutex:
                self.command_queue.queue.clear()
            self.thread = Thread(target=Recorder.__record, args=(self, filename, port, baud, timeout))
            self.thread.start()
            return True

    def stop(self):
        if self.running():
            self.command_queue.put(Command.STOP)
            self.thread.join()
            self.thread = None

    def running(self):
        return self.thread is not None

    def __record(self, filename, port, baud=115200, timeout=1):
        print(f"Beginning to record to {filename}")
        print(f"record({filename}, {port}, {baud}, {timeout})")
        with Serial(port, baudrate=baud, timeout=timeout) as ser:
            with open(filename, 'w') as f:
                ser.readline()  # Ignore first line, likely partially-filled.
                initial_com_time, _, _ = parse_line(ser.readline().strip().decode())
                while True:
                    if not self.command_queue.empty():
                        if self.command_queue.get() == Command.STOP:
                            break
                    line = ser.readline().strip().decode()
                    if line:
                        f.write(line + "\n")
                    if self.plotter is not None:
                        com_time, angle, speed = parse_line(line)
                        com_time -= initial_com_time
                        self.plotter.add_datum((com_time, angle, speed))
        print("Finished recording.")


class Playback:
    def __init__(self, plotter: Plotter = None):
        self.thread = None
        self.command_queue = Queue()
        self.plotter = plotter

    def start(self, filename, port=None, baud=None, timeout=None):
        if self.running():
            mb.showerror("Start Error", "Already running")
            return False
        else:
            with self.command_queue.mutex:
                self.command_queue.queue.clear()
            self.thread = Thread(target=Playback.__playback, args=(self, filename))
            self.thread.start()
            return True

    def stop(self):
        if self.running():
            self.command_queue.put(Command.STOP)
            self.thread.join()
            self.thread = None

    def running(self):
        return self.thread is not None

    def __playback(self, filename):
        print(f"Playing back {filename}")
        with open(filename, 'r') as f:
            f.readline()  # Ignore first line, likely partially-filled.
            initial_file_time, _, _ = parse_line(f.readline().strip())
            initial_real_time = time.time()
            for line in f:
                if not self.command_queue.empty():
                    if self.command_queue.get() == Command.STOP:
                        break
                if self.plotter is not None:
                    file_time, angle, speed = parse_line(line.strip())
                    file_time -= initial_file_time
                    delay = file_time - (time.time() - initial_real_time)
                    if delay > 0:
                        time.sleep(delay)
                    self.plotter.add_datum((file_time, angle, speed))
        print("Finished playback.")


def update_com_ports(repeat=None):
    global com_ports
    com_ports = {str(port): port.name for port in comports()}
    port_strs = [str(key) for key in com_ports.keys()]
    if list(port_o['values']) != port_strs:
        port_o['values'] = port_strs
        if port_o.current() == -1:
            if com_ports:
                port_o.current(0)
            else:
                port_s.set("")

    if repeat is not None:
        port_o.after(repeat, lambda: update_com_ports(repeat))


def browse():
    file = fd.asksaveasfilename(confirmoverwrite=False, defaultextension='log', filetypes=[('log', ['log'])])
    if file:
        filename_t.delete(1.0, tk.END)
        filename_t.insert(tk.END, file)


if __name__ == '__main__':
    plotter = Plotter()
    recorder = Recorder(plotter)
    # recorder = Playback(plotter)
    com_ports = {}

    root = tk.Tk()
    root.title("Serial Logger")

    app = tk.Frame(root)
    app.pack()

    filename_l = tk.Label(app, text="Filename")
    filename_t = tk.Text(app, height=1, width=50)
    browse_img = tk.PhotoImage(master=app, file=r"browse.png").subsample(8, 8)
    filename_b = tk.Button(app, image=browse_img, command=browse)
    port_l = tk.Label(app, text="Device")
    port_s = tk.StringVar(app)
    port_o = ttk.Combobox(app, width=50, textvariable=port_s)
    refresh_img = tk.PhotoImage(master=app, file=r"refresh.png").subsample(20, 20)
    port_b = tk.Button(app, image=refresh_img, command=update_com_ports)


    def start_callback():
        filename_s = filename_t.get("1.0", 'end-1c')
        if not filename_s:
            mb.showerror("File error", "No file provided.")
            return

        selected = port_s.get()
        if not filename_s.endswith('.log'):
            filename_s = filename_s + '.log'

        selected = com_ports.get(selected, selected)
        if plotter.start(filename_s):
            if not recorder.start(filename_s, selected):
                plotter.stop()


    def start_callback_event(event=None):
        start_callback()
        return "break"


    def stop_callback():
        recorder.stop()
        plotter.stop()


    start = tk.Button(app, text="Start", command=start_callback)
    stop = tk.Button(app, text="Stop", command=stop_callback)


    def focus_next_window(event):
        if event.state & 1 == 1:  # If shift is pressed
            focus_widget = event.widget.tk_focusPrev()
        else:
            focus_widget = event.widget.tk_focusNext()
        focus_widget.focus()
        return "break"


    for widget in app.children.values():
        if type(widget) is tk.Text:
            widget.bind("<Tab>", focus_next_window)
            widget.bind("<Return>", start_callback_event)

    filename_l.grid(row=0, column=0, padx=2, pady=2)
    filename_t.grid(row=0, column=1, padx=2, pady=2)
    filename_b.grid(row=0, column=2, padx=2, pady=2)
    port_l.grid(row=1, column=0, padx=2, pady=2)
    port_o.grid(row=1, column=1, padx=2, pady=2)
    port_b.grid(row=1, column=2, padx=2, pady=2)
    start.grid(row=2, column=0, padx=2, pady=2, sticky="news")
    stop.grid(row=2, column=1, padx=2, pady=2, sticky="news")


    def end():
        recorder.stop()
        root.destroy()


    update_com_ports(1000)
    root.protocol("WM_DELETE_WINDOW", end)
    plotter.set_root(root, 100)
    root.after(0, plotter.update)
    plt.show()
    root.mainloop()
